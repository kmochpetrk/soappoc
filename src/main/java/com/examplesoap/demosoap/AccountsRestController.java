package com.examplesoap.demosoap;

import com.example.common.exceptions.ResourceNotFoundException;
import com.examplesoap.demosoap.accounts.generated.GlAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.x500.X500Principal;
import javax.servlet.http.HttpServletRequest;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
public class AccountsRestController {

    @GetMapping("/accountsrest")
    public List<GlAccount> getAccounts() {

//        X509Certificate certs[] =
//                (X509Certificate[])req.getAttribute("javax.servlet.request.X509Certificate");
//// ... Test if non-null, non-empty.
//
//        X509Certificate clientCert = certs[0];
//
//// Get the Subject DN's X500Principal
//        X500Principal subjectDN = clientCert.getSubjectX500Principal();
//
//        log.info("subjectDn: " + subjectDN.getName());

        List<GlAccount> retList = new ArrayList<>();
        GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo("811");
        retList.add(glAccount);
        GlAccount glAccount2 = new GlAccount();
        glAccount2.setAcctNo("821");
        retList.add(glAccount2);
        return retList;
    }

    @GetMapping("/accountsrestwithexc")
    public List<GlAccount> getAccountsExc(HttpServletRequest req, @RequestParam(name = "isexc", required = false) Boolean isExc) {

        if (isExc) {
            throw new ResourceNotFoundException("No account found");
        }

//        X509Certificate certs[] =
//                (X509Certificate[])req.getAttribute("javax.servlet.request.X509Certificate");
//// ... Test if non-null, non-empty.
//
//        X509Certificate clientCert = certs[0];
//
//// Get the Subject DN's X500Principal
//        X500Principal subjectDN = clientCert.getSubjectX500Principal();
//
//        log.info("subjectDn: " + subjectDN.getName());

        List<GlAccount> retList = new ArrayList<>();
        GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo("811");
        retList.add(glAccount);
        GlAccount glAccount2 = new GlAccount();
        glAccount2.setAcctNo("821");
        retList.add(glAccount2);
        return retList;
    }
}
