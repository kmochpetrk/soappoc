package com.examplesoap.demosoap;

import com.examplesoap.demosoap.services.MessageService;
import com.sun.xml.ws.handler.SOAPMessageContextImpl;
import com.sun.xml.ws.api.message.HeaderList;
import com.sun.xml.ws.developer.JAXWSProperties;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;


import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@Slf4j
public class LogMessageHandler implements SOAPHandler<SOAPMessageContext> {


    private MessageService messageService;

    @Override
    public Set<QName> getHeaders() {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        SOAPMessage msg = context.getMessage(); //Line 1
        //log.info(((HeaderList)context.get(JAXWSProperties.INBOUND_HEADER_LIST_PROPERTY)).get(0).getStringContent());
        //((HeaderList)((SOAPMessageContextImpl) context).getMessageContext().get(JAXWSProperties.INBOUND_HEADER_LIST_PROPERTY)).get(0).getStringContent()
        Boolean outbound = (Boolean) context
                .get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if ((outbound != null) && (!outbound.booleanValue())) {
            handleInboundMessage(context);
        } else {
            handleOutboundMessage(context);
        }



        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            msg.writeTo(baos);  //Line 3
            final String baosStr = baos.toString();
            log.info(baosStr);
            messageService.saveMessageReq(baosStr);
        } catch (Exception ex) {
            Logger.getLogger(LogMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    private void handleOutboundMessage(SOAPMessageContext context) {
        try {
            SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
            SOAPFactory factory = SOAPFactory.newInstance();
            String prefix = "http";
            String uri = "http://services.samples";
            SOAPElement corrElem =
                    factory.createElement("correlation_id", prefix, uri);
            corrElem.addTextNode((String) context.get("correlation_id"));
            SOAPHeader header = envelope.getHeader();
            header.addChildElement(corrElem);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleInboundMessage(SOAPMessageContext context) {

        SOAPHeader header = null;
        try {
            header = context.getMessage().getSOAPHeader();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        Iterator<?> headerElements = header.examineAllHeaderElements();
        while (headerElements.hasNext()) {
            SOAPHeaderElement headerElement = (SOAPHeaderElement) headerElements
                    .next();
            if (headerElement.getElementName().getLocalName()
                    .equals("correlation_id")) {
                final String corrId = headerElement.getTextContent();
                MDC.put("correlation_id", corrId);
                log.info("correlation_id: " + corrId);
                MDC.clear();
                context.put("correlation_id", corrId);
            }
        }


    }

    private String getFirstChildElementValue(SOAPElement soapElement, QName qNameToFind) {
        String value = null;
        Iterator<?> it = soapElement.getChildElements(qNameToFind);
        while (it.hasNext()) {
            SOAPElement element = (SOAPElement) it.next(); //use first
            value = element.getValue();
        }
        return value;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return true;
    }

    @Override
    public void close(MessageContext context) {
    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }
}
