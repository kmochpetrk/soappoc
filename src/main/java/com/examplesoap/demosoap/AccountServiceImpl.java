package com.examplesoap.demosoap;

import com.examplesoap.demosoap.accounts.generated.BottomUpServiceInterface;
import com.examplesoap.demosoap.accounts.generated.GlAccount;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceContext;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.*;

@Component
@WebService(endpointInterface = "com.examplesoap.demosoap.accounts.generated.BottomUpServiceInterface")
@HandlerChain(file = "handlers.xml")
public class AccountServiceImpl implements BottomUpServiceInterface {

    @Resource
    private WebServiceContext wsc;

    @Override
    public GlAccount getAccountByAcctno(GlAccount arg0) {

//        Map<QName, List<String>> o = (Map<QName, List<String>>) wsc.getMessageContext().get("jaxws.binding.soap.headers.outbound");
//        if (o == null) {
//            o = new HashMap<QName, List<String>>();
//            wsc.getMessageContext().put("jaxws.binding.soap.headers.outbound", o);
//        }
//        o.put(new QName("http://services.samples", "correlation_id"), Collections.singletonList((String) wsc.getMessageContext().get("correlation_id")));

        GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo(arg0.getAcctNo() + "100");
        return glAccount;
    }

    @Override
    public GlAccount createNew(GlAccount arg0) {
        return arg0;
    }

    @Override
    public List<GlAccount> getAllAccounts() {
        //HandlerUtils.printMessageContext("Web Service Provider", wsc.getMessageContext());

        List<GlAccount> retList = new ArrayList<>();
        GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo("411");
        retList.add(glAccount);
        GlAccount glAccount2 = new GlAccount();
        glAccount2.setAcctNo("421");
        retList.add(glAccount2);
        GlAccount glAccount3 = new GlAccount();
        glAccount3.setAcctNo("521");
        retList.add(glAccount3);

//        BottomUpServiceImplService service = new BottomUpServiceImplService();
//        final BottomUpServiceInterface port = service.getBottomUpServiceImplPort();
//        BindingProvider provider = (BindingProvider)port;
//        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
//                new javax.net.ssl.HostnameVerifier(){
//
//                    public boolean verify(String hostname,
//                                          javax.net.ssl.SSLSession sslSession) {
//                        if (hostname.equals("localhost")) {
//                            return true;
//                        }
//                        return false;
//                    }
//                });
//
//        provider.getRequestContext().put(
//                BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
//                "https://localhost:8443/services/accounts");
//        try {
//            provider.getRequestContext().put("com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory", getCustomSocketFactory());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        final List<GlAccount> allAccounts = port.getAllAccounts();
//        retList.addAll(allAccounts);
        if (retList.size() == 2) {
            throw new IllegalStateException("Just 2 accounts");
        }
        return retList;
    }

    private SSLSocketFactory getCustomSocketFactory() throws Exception {

        // Create and load the truststore
        KeyStore trustStore = KeyStore.getInstance("JKS");
        trustStore.load(DemosoapApplication.class.getClassLoader().getResourceAsStream("jks/soap1truststore.jks"), "Kava123".toCharArray());

        // Create and initialize the truststore manager
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(trustStore);

        // Create and initialize the SSL context
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
        return sslContext.getSocketFactory();
    }

    @Override
    public String getPokus() {
        return "Pokus!!!!!!!!!!!!!!!";
    }
}
