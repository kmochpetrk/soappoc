package com.examplesoap.demosoap.daos;

import com.examplesoap.demosoap.entities.MessageReq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface MessageReqDao extends JpaRepository<MessageReq, Long> {
}
