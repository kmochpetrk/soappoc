package com.examplesoap.demosoap;

import com.examplesoap.demosoap.accounts.generated.BottomUpServiceInterface;
import com.examplesoap.demosoap.services.MessageService;
import com.revinate.ws.spring.SpringService;
import com.sun.xml.ws.transport.http.servlet.SpringBinding;
import com.sun.xml.ws.transport.http.servlet.WSSpringServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchDataSource;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import javax.servlet.Servlet;
import javax.xml.ws.Endpoint;
import javax.xml.ws.handler.Handler;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
//@EnableScheduling
@ComponentScan(basePackages = {"com.examplesoap.demosoap", "com.example.common.exceptions", "com.example.common.logging"})
public class DemosoapApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemosoapApplication.class, args);
    }


    @Autowired
    private BottomUpServiceInterface bottomUpServiceInterface;

    @Autowired
    private MessageService messageService;

    //@Scheduled(initialDelay = 5000, fixedDelay = 1000*60*60*24)
    public void init() {
//        Endpoint.publish("http://localhost:9444/accounts", new AccountServiceImpl());
//
//        System.out.println("EmployeeLookupService Started!");
       for (int i=0; i < 50; i++) {
           messageService.saveMessageReq("1111111xxxxxxxxxxxxxxxxxxxxxxxxxxxxx-yyyyyyyyyyyyyyyyyyyyyyyy-ggggggggggggggggggggg");
       }
    }

    @Bean
    public Servlet servlet() {
        return new WSSpringServlet();
    }

    @Bean
    public ServletRegistrationBean<Servlet> servletRegistrationBean(){
        return new ServletRegistrationBean<Servlet>(servlet(), "/services/*");
    }

    @Bean()
    public SpringBinding springBinding() throws Exception {
        SpringService service=new SpringService();
        List<Handler> handlers = new ArrayList<>();
        final LogMessageHandler messageHandler = new LogMessageHandler();
        messageHandler.setMessageService(messageService);
        handlers.add(messageHandler);
        service.setHandlers(handlers);
        service.setBean(bottomUpServiceInterface);
        SpringBinding binding=new SpringBinding();
        binding.setService(service.getObject());
        binding.setUrl("/services/accounts");
        return binding;
    }

}
