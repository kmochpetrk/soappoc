package com.examplesoap.demosoap.services;

import com.examplesoap.demosoap.daos.MessageReqDao;
import com.examplesoap.demosoap.entities.MessageReq;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
public class MessageService {

    @Autowired
    private MessageReqDao messageReqDao;

    public void saveMessageReq(String messageReq) {
        final MessageReq savedReq = messageReqDao.save(new MessageReq(null, messageReq));
        log.info("Req saved " + savedReq.toString());
    }
}
