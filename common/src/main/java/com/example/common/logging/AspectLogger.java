package com.example.common.logging;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Aspect
@Configuration
@Slf4j
@EnableAspectJAutoProxy
public class AspectLogger {

    @Around("execution(* com.*..*Controller.*(..))")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        ObjectMapper objectMapper = new ObjectMapper();


        String method = joinPoint.getSignature().getName();
        String clazz = joinPoint.getTarget().getClass().toString();
        String args = objectMapper.writeValueAsString(joinPoint.getArgs());

        log.info("Before " + clazz + "." + method + "()" + args);

        final Object proceed = joinPoint.proceed();//continue on the intercepted method

        log.info("after " + clazz + "." + method + "(): " +
                objectMapper.writeValueAsString(proceed));

        return proceed;
    }

}
