package com.example.common.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.contrib.json.classic.JsonLayout;

import java.util.LinkedHashMap;
import java.util.Map;

public class CustomJsonLayout extends JsonLayout {

    public CustomJsonLayout() {
        super();

    }


    @Override
    protected Map toJsonMap(ILoggingEvent event) {

        final AppPropsCrate bean = SpringContext.getBean(AppPropsCrate.class);


        Map<String, Object> map = new LinkedHashMap<String, Object>();

        addTimestamp(TIMESTAMP_ATTR_NAME, this.includeTimestamp, event.getTimeStamp(), map);
        add("type", true, "app_log", map);
        add("schema_version", true, "1.0", map);
        final String correlation_id = event.getMDCPropertyMap().get("correlation_id");
        //add("schema_id", true, correlation_id != null ? correlation_id : "null", map);
        if (bean != null) {
            add("application", true, bean.getApplicationName(), map);
            add("schema_id", true, bean.getUuid(), map);
        }
        add(LEVEL_ATTR_NAME, this.includeLevel, String.valueOf(event.getLevel()), map);
        add("code", this.includeThreadName, event.getThreadName(), map);
        //addMap(MDC_ATTR_NAME, this.includeMDC, event.getMDCPropertyMap(), map);
        //add(LOGGER_ATTR_NAME, this.includeLoggerName, event.getLoggerName(), map);
        add(FORMATTED_MESSAGE_ATTR_NAME, this.includeFormattedMessage, event.getFormattedMessage(), map);
        //add(MESSAGE_ATTR_NAME, this.includeMessage, event.getMessage(), map);
        //add(CONTEXT_ATTR_NAME, this.includeContextName, event.getLoggerContextVO().getName(), map);
        addThrowableInfo(EXCEPTION_ATTR_NAME, this.includeException, event, map);
        addCustomDataToJsonMap(map, event);
        return map;
    }

}
