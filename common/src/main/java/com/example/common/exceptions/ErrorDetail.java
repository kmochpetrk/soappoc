package com.example.common.exceptions;

import lombok.Data;

@Data
public class ErrorDetail {
    private String title;
    private int status;
    private String detail;
    private Long timeStamp;
    private String developerMessage;
}
